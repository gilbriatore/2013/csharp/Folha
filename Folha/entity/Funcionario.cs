﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Folha.entity
{
    class Funcionario
    {
        public int Id { set; get; }
        public string Nome { set; get; }
        public string CPF { set; get; }
        public string Email { set; get; }
        public string Contato { set; get; }
    }
}
